import urllib2
from BeautifulSoup import BeautifulSoup

def messages(url):
    result = []
    # This is HTMLParser bug!
    page = urllib2.urlopen(url).read().replace('"onclick=', '" onclick=')
    soup = BeautifulSoup(page)
    # This is ugly hack! When XML-LOR will be ready?
    for msg in soup.findAll('div', {'class': 'msg'}):
        sign = msg.find('div', {'class': 'sign'})
        fr =  str(sign.contents[0]).strip(' (\n').\
            replace('<s>', '').replace('</s>', '')
        title = msg.find('div', {'class': 'title'}).contents
        if len(title) == 5:
            to = str(title[4].strip().split()[1])
        else:
            to = None
        result.append((fr, to))
    return result

if __name__ == '__main__':
    import sys
    messages(sys.argv[1])
