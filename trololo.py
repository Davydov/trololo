#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import re
import time
import operator

import pydot

import lor

def inc_dic(d, k):
    if d.has_key(k):
        d[k] += 1
    else:
        d[k] = 1


g = pydot.Dot()

if len(sys.argv) == 1:
    print "Usage:\nanalizer.py http://lorurl [file.png] [nots]"
    sys.exit()

url = sys.argv[1]
pic = "out.png"
nots = True
if len(sys.argv) > 2:
    pic = sys.argv[2]
if len(sys.argv) > 3:
    if sys.argv[3] == "ts":
        nots = False
print '!'
messages = lor.messages('%s&page=-1' % url)
time.sleep(1)

## Имя топикстартера:
starter, ansto = messages[0]

edges = {}
stat_to = {}
stat_from = {}

for sender, ansto in messages[1:]:
    inc_dic(stat_from, sender)
    if not ansto:
        if nots:
            continue
        else:
            ansto = starter
    if(sender and ansto
            and sender != "Проверено" and ansto != "Проверено"):
        if sender.find("[") != -1:
            sender = sender.split(":")[1]
        if ansto.find("[") != -1:
            ansto = ansto.split(":")[1]
        if sender.find(">")!=-1 or ansto.find(">") != -1: continue

        inc_dic(stat_to, ansto)
        k = (sender,ansto)
        inc_dic(edges, k)

scores = {}

for nick in stat_to:
    scores[nick] = (float(stat_to[nick])-stat_from[nick])/stat_from[nick]

max_score = max(scores.values())

for nick, score in sorted(scores.items(), key=operator.itemgetter(1),reverse=True):
    percent = int(score/max_score*100)
    hexpercent = int(score/max_score*255)
    if score < 0.3:
        break
    g.add_node(pydot.Node(nick, style='filled', bgcolor = '#%4x0000' % hexpercent))
    print '%s: %0.2f (%d%%, %d replies/%d comments)' % (nick, score, percent, 
                                   stat_to[nick], stat_from[nick])

       

for pair in edges.keys():
#    g.add_edge(pydot.Edge(pair[0], pair[1], label="%d" % edges[pair], weight="%d" % edges[pair], style="setlinewidth(%d)" % edges[pair]))
    g.add_edge(pydot.Edge(pair[0], pair[1], label="%d" % edges[pair], weight="%d" % edges[pair]))

g.write(pic+'.dot')
g.write_png(pic+'.png')
g.write_gif(pic+'.gif')

